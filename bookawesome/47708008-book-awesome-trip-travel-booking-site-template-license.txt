LICENSE CERTIFICATE : Envato Market Item
==============================================

This document certifies the purchase of:
ONE REGULAR LICENSE
as defined in the standard terms and conditions on Envato Market.

Licensor's Author Username:
megadrupal

Licensee:
Balemedia

Item Title:
Book Awesome Trip - Travel Booking Site Template

Item URL:
https://themeforest.net/item/book-awesome-trip-travel-booking-site-template/10571139

Item ID:
10571139

Item Purchase Code:
050e0f7c-26ef-44c4-992c-299dc998f855

Purchase Date:
2018-07-21 10:02:47 UTC

For any queries related to this document or license please contact Help Team via https://help.author.envato.com

Envato Pty. Ltd. (ABN 11 119 159 741)
PO Box 16122, Collins Street West, VIC 8007, Australia

==== THIS IS NOT A TAX RECEIPT OR INVOICE ====

